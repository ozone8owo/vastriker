#!usr/bin/python3

from src.utils import GameCommands
from src.art import Art

game = GameCommands()
art = Art()

if __name__ == '__main__':
    game.clear()
    art.print_intro()
    input()
