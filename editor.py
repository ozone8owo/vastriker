#!usr/bin/python3
'''
This script asks input from the user and adds what they type to the data.
'''
import os

from src.art import Art
from src.classes import Item
from time import sleep

art = Art()


def ask():
    art.prompt("Select what you want to edit", ["item", "exit"])
    option = input("> ")
    while option.lower() not in ["item", "exit"]:
        option = input("> ")

    if option == "item":
        prompt_item()


def prompt_item():
    print("### Type the name of your item ###")
    name = input("> ")
    print("### Type the tooltip of your item ###")
    tooltip = input("> ")
    art.prompt("You have chosen these options for your item.",
               [f"Name: {name}", f"Tooltip: {tooltip}"])
    print("Make changes? [Y/N]")
    final = input("> ")
    while final.lower() not in ["y", "n"]:
        final = input("> ")
    if final.lower() == "y":
        newItem = Item.Item(name, tooltip)
        newItem.write()
    elif final.lower() == "n":
        print()  # Newline
        ask()


if __name__ == "__main__":
    art.print_edit()
    print()  # Newline
    if not os.path.isdir("./data"):
        art.fake_load("Making data directory", 20, 0.05)
        os.makedirs("./data")
        os.makedirs("./data/armors")
        os.makedirs("./data/buffs")
        os.makedirs("./data/characters")
        os.makedirs("./data/enemies")
        os.makedirs("./data/items")
        os.makedirs("./data/spells")
        os.makedirs("./data/weapons")
    art.fake_load("Loading editor", 20, 0.05)
    sleep(1)
    print()  # Newline
    ask()
