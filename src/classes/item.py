#!usr/bin/python3
import json


class Item():
    '''
    A class for an item
    '''

    item = {}

    def __init__(self, name, tooltip):
        '''
        Initialize an item.

        Args:
            name: The name of the object
            tooltip: What will be shown when you analize it

        '''
        self.name = name
        self.tooltip = tooltip
        self.code = self.format(name)
        self.item = self._make()

    @staticmethod
    def format(name):
        '''
        Formats a name string so it can be written to files easily.

        Args:
            name: The name of the string.

        Returns:
            The formatted string.
        '''
        name = name.replace(" ", "_")
        name = name.lower()
        if name.startswith("a_"):
            name = name[2:]
        elif name.startswith("an_"):
            name = name[3:]
        elif name.startswith("the_"):
            name = name[4:]
        return name

    def _make(self):
        item = {
            "name": self.name,
            "tooltip": self.tooltip
        }
        return item

    def write(self):
        '''
        Writes an item to the data.

        Args:
            item: The item dict to write.
        '''
        with open(f"data/items/{self.code}.json", "w+") as f:
            json.dump(self.item, f, sort_keys=True,
                      indent=4, separators=(",", ":"))

    @staticmethod
    def read(code):
        '''
        Reads an item from the data. You need to pass the code, not the name!
        '''
        with open(f"data/items/{code}.json", "r") as f:
            item = json.load(f)
            return Item(item['name'], item['tooltip'])
