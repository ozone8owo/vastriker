#!usr/bin/python3
'''
Utility functions, mostly for high-level compatibility between windows and unix
'''
import os


class GameCommands():
    '''
    Commands commonly used for the game.
    '''
    def __init__(self):
        pass

    def clear(self):
        '''Clear the screen, works on windows and unix'''
        if os.name == 'nt':
            tmp = os.system('cls')
            del tmp
        else:
            tmp = os.system('clear')
            del tmp
