#!usr/bin/python3
'''
Art and other fun stuff!
'''
import sys
import random

from time import sleep


class Art():
    '''
    A class for doing several visual things
    '''
    def __init__(self):
        pass

    def print_intro(self):
        print(r'             ..ooo@@@XXX%%%xx..')
        print(r'          .oo@@XXX%x%xxx..     ` .')
        print(r'        .o@XX%%xx..               ` .')
        print(r'      o@X%..                  ..ooooooo')
        print(r'    .@X%x.                 ..o@@^^   ^^@@                  Welcome to VastrikeR')
        print(r'  .ooo@@@@@@ooo..      ..o@@^          @X%                        Have fun and die~')
        print(r'  o@@^^^     ^^^@@@ooo.oo@@^             %')
        print(r' xzI              ^^^o^^                 %')
        print(r' @@@o     ooooooo^@@^o^@X^@oooooo     .X%x')
        print(r'I@@@@@@@@@XX%%xx  ( o@o )X%x@@@@@@@@@@@@X%x')
        print(r'I@@@@XX%%xx  oo@@@@X% @@X%x   ^^^@@@@@@@X%x')
        print(r' @X%xx     o@@@@@@@X% @@XX%%x  )    ^^@X%')
        print(r'  ^   xx o@@@@@@@@Xx  ^ @XX%%x    xxx')
        print(r'        o@@^^^ooo I^^ I^o ooo   .  x')
        print(r'        oo @^ IX      I   ^X  @^ oo')
        print(r'        IX     U  .        V     IX')
        print(r'         V     .           .     V')

    def print_edit(self):
        print(r'  ___________________')
        print(r'  | _______________ |')
        print(r'  | |XXXXXXXXXXXXX| |')
        print(r'  | |XXXXXXXXXXXXX| |               VastrikeR Editor')
        print(r'  | |XXXXXXXXXXXXX| |                       Make some cool stuff!')
        print(r'  | |XXXXXXXXXXXXX| |')
        print(r'  | |XXXXXXXXXXXXX| |')
        print(r'  |_________________|')
        print(r'      _[_______]_')
        print(r'  ___[___________]___')
        print(r' |         [_____] []|__')
        print(r' |         [_____] []|  \__')
        print(r' L___________________J     \ \___\/')
        print(r'  ___________________      /\\')
        print(r' /                   \    (__)')

    def prompt(self, title, options):
        '''
        Given a title and a list of options, it will print out a cute prompt.
        '''
        title = title.strip()
        promptTitle = f"# {title} #".strip()
        for _ in range(len(promptTitle)):
            print("#", end="")
        print(f"\n{promptTitle}")
        print(self._string_sharp(" ", len(title)))  # Whitespace
        for option in options:
            print(self._string_sharp(f"> {option}", len(title) + 1))
        print(self._string_sharp(" ", len(title)))  # Whitespace
        for _ in range(len(promptTitle)):
            print("#", end="")
        print()  # Newline

    def fake_load(self, message, width, timeout):
        '''
        A fake loading bar with a random time to load.
        '''
        print(message, end="")
        print(" [%s]" % ((" " * width)), end="")  # f-string returns syntax error for some reason
        sys.stdout.flush()
        print("\b" * (width+1), end="")  # return to start of line, after '['
        for _ in range(width):
            sleep(random.uniform((timeout - 0.02), (timeout + 0.02)))
            # update the bar
            print("#", end="")
            sys.stdout.flush()
        print("] Done!")

    def _string_sharp(self, string, compare):
        wrap = compare - len(string.strip())
        # f-string returns syntax error for some reason
        return "# {}{}#".format(string, (" " * (wrap)))
