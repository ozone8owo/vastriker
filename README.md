# VastrikeR v0.0.1
VastrikeR is a terminal game, which tries to be customizable, cross-platform and user friendly.
I made it as a learn-by-doing project for terminal graphics and working with JSON~

## How to play
First install [Python 3.7.2](https://www.python.org/downloads/) and be sure to check the 'add Python to PATH' option in the installer. If you run linux or mac, it's probably already installed.
To test if it's properly installed, open a terminal and type `python`. Be sure to check the version number is correct.

VastrikeR works with a `data` directory. This directory contains all items, characters, enemies, etc. It's not included in the git repo. If you wanna be hardcore, you can add all of it yourself with the edit script, but you can also download a premade one. (WIP)

To play, you can open `run.bat` if you're on windows and `run.sh` if you're on linux or mac!

## Wanna make your own stuff? (WIP)
There is a script for adding things to the data! You can just open `edit.bat` or `edit.sh` and you will be prompted to create anything you want.